import { Component, OnInit } from '@angular/core';
import {Customer} from '../Customer'
import { CustomerService } from '../_services/customer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  private customer : Customer ;
  constructor(private router: Router,private customerService: CustomerService) { }

  ngOnInit() {
      console.log('in init method');
    this.customer = new Customer({firstname: '',lastname:'',email:'',address:'',phoneno:'',password: ''});
 }


 onSubmit(){
  
  this.customerService.create(this.customer)
            .subscribe(
                data => {
                    // set success message and pass true paramater to persist the message after redirecting to the login page
                    
                    this.router.navigate(['/login']);
                },
                error => {
                    //this.alertService.error(error);
                    //this.loading = false;
                    console.log(error);
                });
    }

 

}
