package com.pizza.taranaki.orderservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pizza.taranaki.orderservice.entity.Order;
import com.pizza.taranaki.orderservice.repository.OrderRepository;

@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	public Order getOrderById(Long orderId) {
		return orderRepository.findById(orderId).orElse(null);
	}
	
	public List<Order> getOrdersByStatus(String status){
		return orderRepository.findByOrderStatus(status);
	}
	
	public Order saveOrder(Order order) {
		return orderRepository.save(order);
	}

}
