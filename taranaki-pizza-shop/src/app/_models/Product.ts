import { PizzaItem } from "./PizzaItem";

export type ProductSize = 'small' | 'medium' | 'large';

export interface Product {
  pizza_id?: number;
  prizza_name?: string;
  prizza_desc?: string;
  pizza_image?: string;
  pizza_price?: number;
  currency?: string;
  availableSizes?: PizzaItem[];
  availableBases?: PizzaItem[];
  availableToppings?: PizzaItem[];
  picture?: string;
  base?: PizzaItem;
  size?:PizzaItem;
  toppings?:PizzaItem[];

}


