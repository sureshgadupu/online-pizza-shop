package com.pizza.taranaki.productservice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pizza.taranaki.productservice.entity.Category;
import com.pizza.taranaki.productservice.entity.Pizza_Item;

@Repository
public interface PizzaItemRepository  extends CrudRepository<Pizza_Item, Long>{
	
	public List<Pizza_Item> findByCategory(Category category);

}
