import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';


const routes: Routes = [

  { path : '' , redirectTo: 'products', pathMatch: 'full'},
  { path : 'login', component: LoginComponent } ,
  { path : 'register', component: RegistrationComponent } ,
  { path : 'home', component: HomeComponent } ,
 {
    path: 'products',
    component: HomeComponent
  },
  {
    path: 'products/details/:id',
    component: ProductDetailsComponent
  },
  {
    path: 'products/cart',
    component: ShoppingCartComponent
  }
  

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
