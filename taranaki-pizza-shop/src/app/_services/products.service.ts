import {Injectable} from '@angular/core';
import {Product} from '../_models/product';
import {Observable} from 'rxjs';
import {of} from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';
import { PizzaItem } from '../_models/PizzaItem';
@Injectable()
export class ProductsService {
  private url='http://localhost:8090/';

  constructor(private http: HttpClient) { }

  
  public getProducts(): Observable<Product[]> {
    //return this.products();
    return this.http.get<any[]>(this.url+"/pizza/list");
  }

  public getProduct(id: number): Observable<Product> {

      return this.http.get<any>(this.url+"/pizza/"+id);
  }

  public getPizzaSize(): Observable<PizzaItem[]> {
       return this.http.get<any>(this.url+"/pizzaitems/category/3");
   }

   public getPizzaBase(): Observable<PizzaItem[]> {
    return this.http.get<any>(this.url+"/pizzaitems/category/1");
}

public getPizzaTopping(): Observable<PizzaItem[]> {
  return this.http.get<any>(this.url+"/pizzaitems/category/2");
}

 /* private products(): Observable<Product[]> {
    return of(<Product[]>[
      <Product>{product_id: 1, product_name: 'Blue item', product_price: 123.09},
      <Product>{product_id: 2, product_name: 'Green and gray', product_price: 99.09},
      <Product>{product_id: 3, product_name: 'Green item', product_price: 99.09},
      <Product>{product_id: 4, product_name: 'Blue and gray', product_price: 99.09},
      <Product>{product_id: 5, product_name: 'Green and blue', product_price: 99.09},
      <Product>{product_id: 6, product_name: 'Green and blue', product_price: 99.09},
      <Product>{product_id: 7, product_name: 'Gray', product_price: 99.09},
      <Product>{product_id: 8, product_name: 'Blue', product_price: 99.09},
      <Product>{product_id: 9, product_name: 'All colors', product_price: 99.09},
    ]);
  } */
}
