package com.pizza.taranaki.productservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pizza_Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pizza_item_id;
	
	@Column(nullable=false)
	private String pizza_item_name;
	
	@Column(nullable=false)
	private String pizza_item_image;
	
	@Column(nullable=false)
	private Long pizza_item_price;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="category_id",referencedColumnName="category_id")
	private Category category;

	public Long getPizza_item_id() {
		return pizza_item_id;
	}

	public void setPizza_item_id(Long pizza_item_id) {
		this.pizza_item_id = pizza_item_id;
	}

	public String getPizza_item_name() {
		return pizza_item_name;
	}

	public void setPizza_item_name(String pizza_item_name) {
		this.pizza_item_name = pizza_item_name;
	}

	public String getPizza_item_image() {
		return pizza_item_image;
	}

	public void setPizza_item_image(String pizza_item_image) {
		this.pizza_item_image = pizza_item_image;
	}

	public Long getPizza_item_price() {
		return pizza_item_price;
	}

	public void setPizza_item_price(Long pizza_item_price) {
		this.pizza_item_price = pizza_item_price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	  

}
