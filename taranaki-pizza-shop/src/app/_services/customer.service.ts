import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Customer} from '../Customer'

@Injectable()
export class CustomerService {
 
  private url='http://localhost:8080/customer'
  constructor(private http: HttpClient) { }

  create(customer: Customer) {
    return this.http.post(this.url+'/register', customer);
  }

  update(customer: Customer) {
    return this.http.put(this.url +'/update'+ customer.customerid, customer);
  }
  
  getById(id: number) {
    return this.http.get(this.url + id);
 }

}
