

export const sizes =['Small','Medium','Large'];
export const bases =['FreshPan','Thin Crust','HandTossed'];
export const toppings =['Olives','Tomato','Extra Cheese'];