import { Product } from '../_models/Product';
import {ShoppingCartService} from '../_services/shopping-cart.service';
import {Component} from '@angular/core';
import { ChangeDetectionStrategy,OnInit, OnDestroy, Input} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';



@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductItemComponent implements OnInit {
  
  @Input() public pizza_id: number;
  @Input() public pizza_name: string;
  @Input() public pizza_image: string;
  @Input() public pizza_price: number;
  @Input() public currency: string;
  @Input() public size: number;

  ngOnInit() {
  }

  public getCurrency(): string {
    return '$';
  }

}
