import { Component, OnInit } from '@angular/core';
import { Customer } from '../Customer';
import {Product} from '../_models/Product';
import {ProductsService} from '../_services/products.service' ;

@Component({
  selector: 'app-product-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentCustomer: Customer;
  Products : Product [];
  constructor(private productsServices:ProductsService) { }

  ngOnInit() {
    console.log('in init method');
    this.productsServices.getProducts()
      .subscribe(_ => this.Products = _);
  }

}
