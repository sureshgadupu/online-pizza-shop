package com.pizza.taranaki.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.pizza.taranaki.entity.Customer;
import com.pizza.taranaki.service.CustomerService;

@RestController
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/customer/{id}",method=RequestMethod.GET)
	public Customer getCustomer(@PathVariable String id){
		return customerService.getCustomerById(Long.valueOf(id));
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/customer/authenticate",method=RequestMethod.POST)
	public Customer authenticateCustomer(@RequestBody Customer customer){
		System.out.println(customer.getEmail()+" :"+ customer.getPassword());
		return customerService.findCustomerByEmailAndPassword(customer.getEmail(), customer.getPassword());
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/customer/register",method=RequestMethod.POST)
	public Customer addCustomer(@RequestBody Customer customer){
		 customerService.addCustomer(customer);
		 return  customerService.getCustomerById(customer.getCustomerid());
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/customer/{id}",method=RequestMethod.PUT)
	public Customer updateCustomer(@RequestBody Customer customer,@PathVariable Long id){
		customerService.updateCustomer(customer,id);
		 return customer;
	}

}
