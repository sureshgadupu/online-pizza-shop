package com.pizza.taranaki.productservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pizza_Master {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long    pizza_id;
	
	@Column(nullable=false)
	private String  pizza_name ;
	
	@Column(nullable=false)
	private String  pizza_desc;
	
	@Column(nullable=true)
	private String  pizza_image ;
	
	@Column(nullable=false)
	private Double  pizza_price ;

	public Long getPizza_id() {
		return pizza_id;
	}

	public void setPizza_id(Long pizza_id) {
		this.pizza_id = pizza_id;
	}

	public String getPizza_name() {
		return pizza_name;
	}

	public void setPizza_name(String pizza_name) {
		this.pizza_name = pizza_name;
	}

	public String getPizza_desc() {
		return pizza_desc;
	}

	public void setPizza_desc(String pizza_desc) {
		this.pizza_desc = pizza_desc;
	}

	public String getPizza_image() {
		return pizza_image;
	}

	public void setPizza_image(String pizza_image) {
		this.pizza_image = pizza_image;
	}

	public Double getPizza_price() {
		return pizza_price;
	}

	public void setPizza_price(Double pizza_price) {
		this.pizza_price = pizza_price;
	}  
	  

}
