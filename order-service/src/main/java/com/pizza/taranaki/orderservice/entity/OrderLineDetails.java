package com.pizza.taranaki.orderservice.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class OrderLineDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long order_line_details_id;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_line_id")
	private OrderLine orderLine;
	 
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pizza_item_id")
	private PizzaItem pizzaItem;

	public Long getOrder_line_details_id() {
		return order_line_details_id;
	}

	public void setOrder_line_details_id(Long order_line_details_id) {
		this.order_line_details_id = order_line_details_id;
	}

	public OrderLine getOrderLine() {
		return orderLine;
	}

	public void setOrderLine(OrderLine orderLine) {
		this.orderLine = orderLine;
	}

	public PizzaItem getPizzaItem() {
		return pizzaItem;
	}

	public void setPizzaItem(PizzaItem pizzaItem) {
		this.pizzaItem = pizzaItem;
	}

	

	

}
