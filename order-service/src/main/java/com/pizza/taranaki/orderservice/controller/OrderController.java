package com.pizza.taranaki.orderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pizza.taranaki.orderservice.entity.Order;
import com.pizza.taranaki.orderservice.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/order/{id}",method=RequestMethod.GET)
	public Order getOrder(@PathVariable Long id){
		return orderService.getOrderById(id);
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/order",method=RequestMethod.POST)
	public Order saveOrder(@RequestBody Order order){
		return orderService.saveOrder(order);
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/order/status/{orderstatus}",method=RequestMethod.POST)
	public List<Order> getOrdersByStatus(@PathVariable String status){
		return orderService.getOrdersByStatus(status);
		
	}
}
