import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
  private apiurl ='http://localhost:8080/customer/authenticate';
  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>(this.apiurl, { email: username, password: password })
        .map(customer => {
            
            console.log(customer);
            if (customer ) {
                // store user details in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentCustomer', JSON.stringify(customer));
            }

            return customer;
        });
}

logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentCustomer');
}

}
