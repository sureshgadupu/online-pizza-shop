package com.pizza.taranaki.productservice;



import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				  .select()                                  
		          //.apis(RequestHandlerSelectors.basePackage("com.demo.springboot.dept"))
				  .apis(RequestHandlerSelectors.any())
		          //.paths(PathSelectors.any())
				  .paths(PathSelectors.regex("(?!!/pizza*).+")) .paths(PathSelectors.regex("(?!/error).+"))
				 // .paths(PathSelectors.ant("/pizza/*")).paths(PathSelectors.ant("/pizzaitem/*"))
				 // .paths(PathSelectors.regex("(?!/error).+")).paths(PathSelectors.regex("(?!/pizza*).+"))
		          .build();  
	}
	
	
	private ApiInfo apiInfo() {
	     return new ApiInfo(
	       "My Product Service API", 
	       "Some custom description of API.", 
	       "API TOS", 
	       "Terms of service", 
	       new Contact("Suresh Gadupu", "www.example.com", "sgadupu@company.com"), 
	       "License of API", "API license URL", Collections.emptyList());
	}

}
