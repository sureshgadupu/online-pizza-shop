import { Component, OnInit } from '@angular/core';
import { Product } from '../_models/Product';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../_services/products.service';
import { ShoppingCartService } from '../_services/shopping-cart.service';
import {bases} from '../_models/data'
import {sizes} from '../_models/data'
import {toppings} from '../_models/data'
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  public product: Product = {};
  constructor(private route: ActivatedRoute
    , private router: Router
    , private productsService: ProductsService
    , private cartService: ShoppingCartService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      const id = +params['id'];
      this.productsService
        .getProduct(id)
        .subscribe(_ => this.product = _)
    });

    this.productsService
        .getPizzaSize().subscribe(_ => this.product.availableSizes=_); 
    this.productsService
        .getPizzaBase().subscribe(_ => this.product.availableBases=_); 
    this.productsService
        .getPizzaTopping().subscribe(_ => this.product.availableToppings=_); 
       
  }
  updateChecked(PizzaItem, $event){
    if($event.isChecked){
      
    }
  }
  onBaseChange(PizzaItem){
    this.product.base = PizzaItem;
  }

  onSizeChange(PizzaItem){
    this.product.size = PizzaItem;
  }

  public addToCart(product: Product) {
    console.log("inside add to cart");
    console.log(product.pizza_id + ";"+product.base);
    
    this.cartService.addToCart(product);
    this.router.navigateByUrl('/products');
  }

}
