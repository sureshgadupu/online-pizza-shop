import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http'


import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { AuthenticationService } from './authentication.service';
import { HomeComponent } from './home/home.component';
import { CustomerService } from './_services/customer.service';
import { ProductsService } from './_services/products.service';
import { ProductItemComponent } from './product-item/product-item.component';
import { ShoppingCartService } from './_services/shopping-cart.service';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';





@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    ProductItemComponent,
    ProductDetailsComponent,
    ProductListComponent,
    ShoppingCartComponent
    
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule ,
    HttpClientModule     
  ],
  providers: [HttpClientModule,AuthenticationService, CustomerService, ProductsService, ShoppingCartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
