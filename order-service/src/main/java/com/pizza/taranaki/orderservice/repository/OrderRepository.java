package com.pizza.taranaki.orderservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pizza.taranaki.orderservice.entity.Order;



public interface OrderRepository extends CrudRepository<Order, Long> {
	
	@Query
	public List<Order> findByOrderStatus(String status);

}
