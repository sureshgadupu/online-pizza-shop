package com.pizza.taranaki.productservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pizza.taranaki.productservice.entity.Pizza_Master;



@Repository
public interface PizzaMasterRepository extends CrudRepository<Pizza_Master, Long>{ 

}
