package com.pizza.taranaki.orderservice.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class OrderLine {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long order_line_id;
	 
	 @ManyToOne
	 @JoinColumn(name = "order_id")
	 private Order order; 
	 
	 @ManyToOne
	 @JoinColumn(name = "pizza_id")
	 private PizzaMaster pizza; 
	 
	 @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	 @JoinColumn(name = "order_line_id")
	 private List<OrderLineDetails> orderLineDetailsList =new ArrayList<>();

	public Long getOrder_line_id() {
		return order_line_id;
	}

	public void setOrder_line_id(Long order_line_id) {
		this.order_line_id = order_line_id;
	}

	

	
	public List<OrderLineDetails> getOrderLineDetailsList() {
		return orderLineDetailsList;
	}

	public void setOrderLineDetailsList(List<OrderLineDetails> orderLineDetailsList) {
		this.orderLineDetailsList = orderLineDetailsList;
	}

	public PizzaMaster getPizza() {
		return pizza;
	}

	public void setPizza(PizzaMaster pizza) {
		this.pizza = pizza;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	 
}
