import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Product } from '../_models/Product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListComponent implements OnInit {

  @Input() public items: Product[] = [];
  constructor() { }

  ngOnInit() {
  }

}
