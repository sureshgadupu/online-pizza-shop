package com.pizza.taranaki.productservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pizza.taranaki.productservice.entity.Category;
import com.pizza.taranaki.productservice.entity.Pizza_Item;
import com.pizza.taranaki.productservice.entity.Pizza_Master;
import com.pizza.taranaki.productservice.repository.PizzaItemRepository;
import com.pizza.taranaki.productservice.repository.PizzaMasterRepository;

@Service
public class ProductService {
	
	@Autowired
	private PizzaMasterRepository pizzaMasterRepository;
	
	@Autowired
	private PizzaItemRepository pizzaItemRepository;
	
	/*@Autowired
	private PizzaBaseMasterRepository pizzaBaseMasterRepository;
	
	@Autowired
	private PizzaSizeMasterRepository pizzaSizeMasterRepository;
	
	@Autowired
	private PizzaToppingMasterRepository pizzaToppingMasterRepository;*/
	
	public List<Pizza_Master> getAllPizzas(){
		List<Pizza_Master> pizzaList =  new ArrayList<>();
		pizzaMasterRepository.findAll().forEach(pizzaList::add);
		return pizzaList;
	}
	
	public Pizza_Master getPizzaById(Long id) {
		
		return pizzaMasterRepository.findById(id).orElse(null);
	}
	
	public List<Pizza_Item> getPizzaItemsByCategory(Category category){
		 return pizzaItemRepository.findByCategory(category);		
	}
	
	/*public List<PizzaBase_Master> getAllPizzaBases(){
		List<PizzaBase_Master> pizzaBaseList =  new ArrayList<>();
		pizzaBaseMasterRepository.findAll().forEach(pizzaBaseList::add);
		return pizzaBaseList;
	}
	
	public PizzaBase_Master getPizzaBaseById(Long id) {
		
		return pizzaBaseMasterRepository.findById(id).orElse(null);
	}
	
	
	public List<PizzaSize_Master> getAllPizzaSizes(){
		List<PizzaSize_Master> pizzaSizeList =  new ArrayList<>();
		pizzaSizeMasterRepository.findAll().forEach(pizzaSizeList::add);
		return pizzaSizeList;
	}
	
	public PizzaSize_Master getPizzaSizeById(Long id) {
		
		return pizzaSizeMasterRepository.findById(id).orElse(null);
	}
	
	public List<PizzaTopping_Master> getAllPizzaToppings(){
		List<PizzaTopping_Master> pizzaToppingList =  new ArrayList<>();
		pizzaToppingMasterRepository.findAll().forEach(pizzaToppingList::add);
		return pizzaToppingList;
	}
	
	public PizzaTopping_Master getPizzaToppingById(Long id) {
		
		return pizzaToppingMasterRepository.findById(id).orElse(null);
	}*/
	

}
