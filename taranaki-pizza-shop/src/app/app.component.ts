import { Observable } from 'rxjs/Observable';
import { Product } from './_models/Product';
import { ShoppingCartService } from './_services/shopping-cart.service';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'Taranaki Pizza Shop';
  public shoppingCartItems$: Observable<Product[]>;
  
  constructor(public location: Location
    , private cartService: ShoppingCartService) {
  
      this.shoppingCartItems$ = this
        .cartService
        .getItems();
  
      this.shoppingCartItems$.subscribe(_ => _);
    }
}
