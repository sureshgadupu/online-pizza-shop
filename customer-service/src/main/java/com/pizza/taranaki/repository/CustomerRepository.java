package com.pizza.taranaki.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.pizza.taranaki.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	public Customer findByEmail(String email);
	public Customer findByEmailAndPassword(String email,String password);

}
