package com.pizza.taranaki.productservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pizza.taranaki.productservice.entity.Category;
import com.pizza.taranaki.productservice.entity.Pizza_Item;
import com.pizza.taranaki.productservice.entity.Pizza_Master;
import com.pizza.taranaki.productservice.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value="Product-Service", description="Operations pertaining to products in Pizza Store")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	
	@ApiOperation(value = "View a list of available pizzas",response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    }
    )
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizza/list",method=RequestMethod.GET)	
	public List<Pizza_Master> getAllPizzas(){
		return productService.getAllPizzas();
		
	}
	
	@ApiOperation(value = "View  pizza By Id",response = Pizza_Master.class)
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizza/{id}",method=RequestMethod.GET)	
	public Pizza_Master getPizza(@PathVariable Long id){
		return productService.getPizzaById(id);
		
	}
	
	@ApiOperation(value = "View  pizza items  By CategoryId",response = List.class)
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizza/items/category/{categoryId}",method=RequestMethod.GET)	
	public List<Pizza_Item> getPizzaItemsByCategory(@PathVariable Long categoryId){
		Category category = new Category();
		category.setCategory_id(categoryId);
		return productService.getPizzaItemsByCategory(category);
		
	}
	
	
	
	
	/*@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizzabase/list",method=RequestMethod.GET)	
	public List<PizzaBase_Master> getAllPizzaBases(){
		return productService.getAllPizzaBases();
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizzasize/list",method=RequestMethod.GET)	
	public List<PizzaSize_Master> getAllPizzaSizes(){
		return productService.getAllPizzaSizes();
		
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/pizzatopping/list",method=RequestMethod.GET)	
	public List<PizzaTopping_Master> getAllPizzaToppings(){
		return productService.getAllPizzaToppings();
		
	}*/

}
