package com.pizza.taranaki.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pizza.taranaki.entity.Customer;
import com.pizza.taranaki.repository.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;

	public Customer getCustomerById(Long customerid) {
		//Optional<Entity> aThing = customerRepository.findById(1L);
		return customerRepository.findById(customerid).orElse(null);
		
	}
	
	public Customer findCustomerByEmailAndPassword(String email,String  password) {
		//Optional<Entity> aThing = customerRepository.findById(1L);
		return customerRepository.findByEmailAndPassword(email, password);
		
	}

	public Customer addCustomer(Customer customer) {
				
		return customerRepository.save(customer);
		
	}

	public String updateCustomer(Customer customer, Long id ) {
		
		if(customerRepository.existsById(id)) {
			customerRepository.save(customer);
			return "Customer updated successfully";
		}
		return "No customer found with Id:"+ id;
		
	}

}
