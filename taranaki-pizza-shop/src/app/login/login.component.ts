import { Component, OnInit } from '@angular/core';
import {Login} from '../Login'
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private login : Login ;
  constructor(private authenticationService:AuthenticationService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.login = new Login({email:'',password: ''});
  }

  onSubmit(){
    console.log(this.login.email+" :"+ this.login.password);
    this.authenticationService.login(this.login.email, this.login.password)
            .subscribe(
                data => {
                    //this.router.navigate([this.returnUrl]);
                    console.log('in success');
                    this.router.navigate(['/home']);
                },
                error => {
                    
                    console.log('error occured');
                });
  }

}
