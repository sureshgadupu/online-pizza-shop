export class Customer {

    customerid:number;
    firstname: string ;
    lastname:string;
    email : string;
    address : string;
    password : string;
   /* password : {
      pwd:string;
      confirmPwd:string;
    }*/

    phoneno:string;
    gender:string;
    tnc:boolean;
  
    constructor(values: Object = {}) {
      //Constructor initialization
      Object.assign(this, values);
    }
  }
  